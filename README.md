# mutf84rs

Support for making use of modified UTF-8 strings in Rust. Typically used for parsing Java class files.
