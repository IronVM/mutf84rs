/* SPDX-License-Identifier: GPL-2.0
 *
 * mutf84rs: Java mutf8 strings for Rust
 * Copyright (C) 2022 HTGAzureX1212.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

//! Modified UTF-8 string slices.

use std::mem;

mod validation;

/// A string slice in modified UTF-8.
#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct Mutf8Str {
    array: [u8],
}

/// Converts a slice of bytes into a Modified UTF-8 string slice without checking that the source
/// slice contains valid Modified UTF-8.
///
/// # Safety
///
/// The bytes passed in must be valid Modified UTF-8.
#[inline]
#[must_use]
pub const unsafe fn from_mutf8_unchecked(v: &[u8]) -> &Mutf8Str {
    unsafe {
        // SAFETY: the caller must guarantee that the bytes `v` are valid Modified UTF-8.
        // this also relies on `&MStr` and `&[u8]` having the same layout.
        mem::transmute(v)
    }
}

/// Converts a slice of bytes into a Modified UTF-8 string slice without checking that the source
/// slice contains valid Modified UTF-8; mutable version.
///
/// See the immutable version, [`from_mutf8_unchecked()`] for more information.
#[inline]
#[must_use]
pub const unsafe fn from_mutf8_unchecked_mut(v: &mut [u8]) -> &mut Mutf8Str {
    // SAFETY: the caller must guarantee that the bytes `v` are valid Modified UTF-8, thus the
    // cast to `*mut Mutf8Str` is safe.
    // The pointer dereference is safe because that pointer comes from a mutable reference which is
    // guaranteed to be valid for writes.
    unsafe { &mut *(v as *mut [u8] as *mut Mutf8Str) }
}
